        private readonly SystemContext _systemContext;
		private readonly IGameTagsModel _gameTagsModel;
		private readonly IBankItemController _bankItemController;

		public RestorePurchasesSystem(SystemContext systemContext, IGameTagsModel gameTagsModel, IBankItemController bankItemController)
		{
			_systemContext = systemContext;
			_gameTagsModel = gameTagsModel;
			_bankItemController = bankItemController;
		}
		
		public void Initialize()
		{
			_gameTagsModel.TagsChanged += Process;
		}

		public void TearDown()
		{
			_gameTagsModel.TagsChanged -= Process;
		}
		
		protected override IEnumerable<IMonitor> Subscribe()
		{
			yield return _systemContext.AllOf<CRestoredPurchases>().OnAdded(Process);
		}

		private void Process()
		{
			_bankItemController.ProcessPendingPurchasesAsync().Forget();
		}
       
       //////////////////////////////////////////////////////////////////////////////////////////////////
       
       
        public async UniTask EnterAsync<T, TPayload>(TPayload payload, CancellationToken ct) where T : class, TState, IPayloadedState<TPayload>
		{
			await UniTask.DelayFrame(1, cancellationToken: ct);

			UniTask ExecuteAsync(TState state, CancellationToken ctt) => CallEnterAsync(state, TypeOf<TPayload>.Raw, payload, ctt);

			await EnterInternalAsync<T>(payload, ExecuteAsync, ct);
		}

		private async UniTask EnterInternalAsync<T>(object payload, Func<TState, CancellationToken, UniTask> callEnter, CancellationToken ct)
			where T : class, TState
		{
			if (IsActive<T>())
			{
				return;
			}

			var oldState = GetStateInstance(CurrentStateType);
			var newState = GetStateInstance(TypeOf<T>.Raw);

			Debug.Log($"[{TypeOf<TState>.Name}] Change state {oldState?.GetType().Name ?? "n/a"} -> {newState.GetType().Name}");
			
			OnBeforeStateChanged?.Invoke(oldState, newState, payload);

			if (oldState is IExitState exitState)
			{
				await exitState.OnExitAsync(ct);
			}

			CurrentState = newState;
			CurrentStateType = newState.GetType();

			await callEnter(CurrentState, ct);

			OnAfterStateChanged?.Invoke(oldState, newState, payload);
		}

		private static async UniTask CallEnterAsync(TState newState, CancellationToken ct)
		{
			if (newState is IEnterState enterState)
			{
				await enterState.OnEnterAsync(ct);
			}
		}

		private static async UniTask CallEnterAsync(TState newState, Type payloadType, object payload, CancellationToken ct)
		{
			var stateGenericType = StateHelpers.PayloadedStateType.MakeGenericType(payloadType);

			if (!stateGenericType.IsInstanceOfType(newState))
			{
				throw new StateException($"State {newState.GetType().FullName} mut be instance of IPayloadedState<{payloadType.Name}>!");
			}

			var mEnterMethod = stateGenericType.GetMethod("OnEnterAsync");
			if (mEnterMethod == null)
			{
				throw new StateException($"State {newState.GetType().FullName}: cannot find OnEnter method!");
			}

			var task = (UniTask)mEnterMethod.Invoke(newState, new[] {payload, ct});
			await task;
		}